import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.firefox.options import Options

"""
Clases Wrapper para usar Selenium.
Desarrollado por Jonathan Ascencio
"""

class TypeBy(By):
  pass

class Daedalus:
  _driver: webdriver
  _raise_error: bool
  _num: int = 1

  def __init__(self, raise_error: bool = False):
    self._raise_error = raise_error

  def open_browser(self, browser: str, url: str = '', path_download: str = None):
    result: bool
    try:
      if "".__eq__(browser):
        raise Exception("Select browser: chrome, firefox, ie")

      if browser == "firefox-hide":
        firefox_options = Options()
        firefox_options.headless = True
        firefox_options.add_argument("--window-size=1920,1080")
        if path_download:
          print(path_download)
          firefox_options.set_preference("browser.download.folderList", 2)
          firefox_options.set_preference("browser.download.dir", path_download)
          firefox_options.set_preference("browser.download.useDownloadDir",True)
          firefox_options.set_preference("browser.helperApps.neverAsk.saveToDisk","application/pdf")
          firefox_options.set_preference("pdfjs.disabled", True)

        self._driver = webdriver.Firefox(options=firefox_options)
        #self._driver = webdriver.Firefox(executable_path="/home/sistemas/bots/geckodriver", options=firefox_options)

      result = True

    except Exception as e:
      self.log_error(f'EXCEPT - open_browser: {browser} - {url} - {str(e)}')
      result = False
      if self._raise_error:
        print(e)
        raise e
        
    return result

  def open_url(self, url: str, seconds_before: float = 0, seconds_after: float = 0):
    result: bool
    try:
      if seconds_before > 0:
         time.sleep(seconds_before)
      
      self._driver.get(url)

      if seconds_after > 0:
        time.sleep(seconds_after)

      result = True
    except Exception as e:
      self.log_error('EXCEPT - open_url: ' + url)
      result = False
      if self._raise_error:
        print(e)
        raise e
    
    return result

  def quit(self):
    try:
      self._driver.quit()
    except Exception as e:
      self.log_error('EXCEPT - quit')
      if self._raise_error:
        print(e)
        raise e
      
  def page_source(self):
    return self._driver.page_source

  def click_object(self, type: By, value: str, seconds_before: float = 0, seconds_after: float = 0):
    result: bool
    try:
      if seconds_before > 0:
         time.sleep(seconds_before)

      self._driver.find_element(by=type, value=value).click()

      if seconds_after > 0:
        time.sleep(seconds_after)

      result = True
    except Exception as e:
      self.log_error(f'EXCEPT - click_object - {str(e)}')
      result = False
      if self._raise_error:
        print(e)
        raise e

    return result

  def wait_object(self, type: By, value: str, timeout: float = 0):
    result: bool
    try:
      wait = WebDriverWait(driver=self._driver, timeout=timeout)
      wait.until(ec.visibility_of_all_elements_located((type, value)))
      result = True
    except Exception as e:
      self.log_error(f'EXCEPT - wait_object: {value} - {str(e)}')
      result = False
      if self._raise_error:
        print(e)
        raise e
    
    return result

  def send_keys(self, type: By, value: str, keys: str, seconds_before: float = 0, seconds_after: float = 0):
    result: bool
    try:
      if seconds_before > 0:
         time.sleep(seconds_before)

      self._driver.find_element(by=type, value=value).send_keys(keys)

      if seconds_after > 0:
        time.sleep(seconds_after)

      result = True
    except Exception as e:
      self.log_error('EXCEPT - send_keys: ' + value + ' ' + keys)
      result = False
      if self._raise_error:
        print(e)
        raise e
    
    return result

  def clear(self, type: By, value: str, seconds_before: float = 0, seconds_after: float = 0):
    result: bool
    try:
      if seconds_before > 0:
        time.sleep(seconds_before)

      self._driver.find_element(by=type, value=value).clear()

      if seconds_after > 0:
        time.sleep(seconds_after)

      result = True
    except Exception as e:
      self.log_error('EXCEPT - clear: ' + value)
      result = False
      if self._raise_error:
        print(e)
        raise e

    return result

  def execute_javascript(self, code: str):
    result: any
    try:
      return self._driver.execute_script(code)
    except Exception as e:
      self.log_error('EXCEPT - execute_javascript ' + str(e))
      result = None
      if self._raise_error:
        print(e)
        raise e

    return result

  # Log de error
  def log_error(self, text, images: bool = False):
    print("\033[91m {}\033[00m".format(text))
    if (images):
      filename = 'imagen_' + '{:03}'.format(self._num) + '.png'
      self.screenshot(f'./imagenes/{filename}')
      self._num += 1

  def log_info(self, text, images: bool = False):
    print("\033[96m {}\033[00m".format(text))
    if (images):
      filename = 'imagen_' + '{:03}'.format(self._num) + '.png'
      self.screenshot(f'./imagenes/{filename}')
      self._num += 1

  def wait(self, time_sleep: int):
    time.sleep(time_sleep)

  