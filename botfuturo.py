import os
from daedalus import Daedalus, By
import time
import schedule

def process_bot():
    total_days_previews = 0 #int(params[1]) 
    total_days_downloads = 8 #int(params[2]) 
    path_download = "/home/sistemas/public_html/bots/dfuturo" #params[3] 

    username = "CF0231"
    password = "NoPerdereMiClave2024$"
    url_telefonica = "https://telefonica-pe.etadirect.com/"
    url_import = "https://back.sistemastest.xyz/importfuture"

    xpath_username = "//*[@id='username']"
    xpath_password = "//*[@id='password']"
    xpath_signin = "//*[@id='sign-in']"
    xpath_close_session = "//*[@id='del-oldest-session'][text()='Suprimir la sesión y conexión de usuario más antiguas']"
    xpath_check_session = "//*[@id='delsession']"
    xpath_logo_login = "//div[@id='ofs-main']//header/div[@class='logo-region']"
    xpath_filter_view = "//button[@title='Vista']"
    xpath_preview = "//button[@class='app-button app-button--icon-only app-button--ghost app-button--transparent'][@title='Anterior']"
    xpath_next = "//button[@class='app-button app-button--icon-only app-button--ghost app-button--transparent'][@title='Siguiente']"
    xpath_check_all_data = "//*[@id='ui-id-91|cb']"
    xpath_apply = "//button[@title='Aplicar']"
    xpath_filter_actions = "//button[@title='Acciones']"
    xpath_export = "//button[@class='toolbar-menu-button menu-item'][@title='Exportar']"
    xpath_submit = "submit-button"
    xpath_message = "message"


    xpath_file = "file-input"
    xpath_response = "//*[@id='submit-button']"

    robot = Daedalus(False)
    robot.open_browser("firefox-hide", path_download=path_download)
    robot.open_url('https://back.sistemastest.xyz/truncatefuture', 0, 5)
    robot.wait(30)
    robot.open_url(url_telefonica, 0, 10)

    login = True

    while login:
        robot.clear(By.XPATH, xpath_username)
        robot.clear(By.XPATH, xpath_password)
        robot.send_keys(By.XPATH, xpath_username, username)
        robot.send_keys(By.XPATH, xpath_password, password)
        robot.click_object(By.XPATH, xpath_signin, 1, 0)

        if robot.wait_object(By.XPATH, xpath_close_session, 5):
            robot.click_object(By.XPATH, xpath_check_session)

        if robot.wait_object(By.XPATH, xpath_logo_login, 5):
            login = False

    robot.wait(10)
    robot.click_object(By.XPATH, xpath_filter_view, 1, 0)
    robot.click_object(By.XPATH, xpath_check_all_data, 1, 1)
    robot.execute_javascript("document.getElementsByClassName('app-button app-button--cta app-button--opaque')[1].click()")

    for i in range(total_days_previews):
        print(f"Retrasar {i} dias")
        robot.click_object(By.XPATH, xpath_preview, 1, 1)

    for i in range(total_days_downloads):
        print(f"Descarga {i}")
        if i > 0:
            print(f"Siguiente {i}")
            robot.click_object(By.XPATH, xpath_next, 1, 1)

        robot.click_object(By.XPATH, xpath_filter_actions, 5, 0)
        robot.click_object(By.XPATH, xpath_export, 1, 0)
        robot.wait(10)

    list_csv = [file for file in os.listdir(path_download) if file.endswith('.csv')]
    list_csv = [(file, os.path.getmtime(os.path.join(path_download, file))) for file in list_csv]
    list_csv.sort(key=lambda x: x[1], reverse=True)
    new_list = list_csv[:total_days_downloads]
    new_list = new_list[::-1]

    for i in range(total_days_downloads):
        print(f"Subida {i}")
        file_name = new_list[i][0]
        robot.open_url(url_import)
        robot.wait(5)
        file_upload_path = path_download + "/"+ file_name
        print(file_upload_path)
        robot.send_keys(By.ID, xpath_file, file_upload_path)
        robot.click_object(By.ID, xpath_submit, 2, 1)
        message = True
        robot.wait(20)
        #print(robot.page_source())
        #while message:
        #    if robot.wait_object(By.ID, xpath_message, 0):
        #        message = False

    robot.quit()

def run_task():
    schedule.every(6).hour.do(process_bot)
    while True:
        schedule.run_pending()
        time.sleep(1)

if __name__ == "__main__":
    run_task()
