import time
import schedule

def tarea():
    print("Tarea ejecutada")

def ejecutar_tarea():
    schedule.every(1).minutes.do(tarea)
    while True:
        schedule.run_pending()
        time.sleep(1)

if __name__ == "__main__":
    ejecutar_tarea()
