from selenium import webdriver
from selenium.webdriver.firefox.options import Options

# Especifica la ruta del geckodriver (si es necesario)
#geckodriver_path = "/home/sistemas/bots/geckodriver"
# En este ejemplo, asumimos que geckodriver está en el PATH, por lo que no necesitamos especificar la ruta.

firefox_options = Options()
firefox_options.headless = True
firefox_options.add_argument("--window-size=1920,1080")
firefox_options.set_preference("browser.download.folderList", 2)
firefox_options.set_preference("browser.download.dir", "./dfuturo/")
firefox_options.set_preference("browser.download.useDownloadDir",True)
firefox_options.set_preference("browser.helperApps.neverAsk.saveToDisk","application/pdf")
firefox_options.set_preference("pdfjs.disabled", True)

driver = webdriver.Firefox(options=firefox_options)

# Crea una instancia del controlador de Firefox
#driver = webdriver.Firefox()#(executable_path=geckodriver_path)


# URL de la página web que deseas consultar
url = "https://telefonica-pe.etadirect.com/"

# Abre la página web en el navegador Firefox
driver.get(url)

# Obtiene el HTML de la página web
html = driver.page_source

# Imprime el HTML obtenido
print(html)

# Cierra el navegador
driver.quit()

